<?php require "include/header.php"?>
            <main class="signup-page">
                <section class="p-content">
                    <p>sign up now</p>
                    <i class="fas fa-sign-in-alt fa-10x"></i>
                </section>  
                <section class="f-signup">
                    <form  name="signupform" action="include/signup.inc.php" method="post" onsubmit="return crousa();">
                        <div class="hint">
                            <input type="text" name="username" placeholder="User Name"  />
                          <?php
                            if(isset($_GET['error']) && $_GET['error'] == 'usernameerror'){
                                echo "<label id='v-unm'>username cant contain symbol or be < 6 char or > 15char</label>";
                            }
                          ?>
                        </div>
                        <div class="hint">
                            <input type="text" name="email" placeholder="E-mail Address"   />
                            <?php
                             if(isset($_GET['error']) && $_GET['error'] == 'invalidemail'){
                                echo "<label id='v-eml'>invalid email</label>";
                            }
                            ?>
                        </div>
                        <div class="hint">
                            <input type="password" name="pwd" placeholder="password"  />
                            <?php
                            if(isset($_GET['error'])&&$_GET['error'] == 'weakpassword' ){
                            echo "<label id='v-pwd'>weack password</label>";
                            }
                            ?>
                        </div>
                        <div class="hint">
                            <input type="password" name="pwdc" placeholder="confirm password"  />
                           <?php
                           if(isset($_GET['error'])&&$_GET['error'] == 'notmatch'){
                            echo "<label id='v-pwdc'> password not matched</label>";
                           }
                            ?>
                        </div>
                        <div class="hint">
                            <button type="submit" name="signupsubmit">signup</button>
                            <?php
                            if(isset($_GET['signup']) && $_GET['signup'] == 'success'){
                            echo "<label id='v-submit'><p>singup success</p></label>";
                            }
                            else if(isset($_GET['error'])&& $_GET['error'] == 'usernameoremailtaken'){
                            echo "<label id='v-submit'>user name or email is taken</label>";
                            }
                            else if(isset($_GET['error']) && ($_GET['error'] == 'serror' || $_GET['error'] == 'sIerror')){
                                header("location: index.php");
                                exit();
                            }
                            ?>
                        </div>
                    </form>
                </section>
                
            </main>
<?php require "include/footer.php"?>