<?php
if(isset($_POST['login'])){
    require "dbcon.inc.php";
    $uemail = $_POST['useremail'];
    $upwd = $_POST['userpwd'];
    if(empty($uemail) || empty($upwd)){
        header("location : ../index.php?emptyfields");
        exit();
    }
    else{
        $uquery = "SELECT * FROM user WHERE email=? OR username=? LIMIT 1;";
        $ustmt = mysqli_stmt_init($conn);
        if(!mysqli_stmt_prepare($ustmt,$uquery)){
            header('location: ../index.php?error=serror');
            exit;
        }
        else{
            mysqli_stmt_bind_param($ustmt,'ss',$uemail,$uemail);
            mysqli_stmt_execute($ustmt);
            $uresult = mysqli_stmt_get_result($ustmt);
            if($info = mysqli_fetch_assoc($uresult)){
            $checkpwd = password_verify($upwd , $info['PASSWORD']);
            if($checkpwd == false || $checkpwd !== true){
               // header('location: ../index.php?error=wrongpass');
               var_dump($info); 
               exit();
            }else if( $checkpwd == true){
                session_start();
               /* if($info['ID'] != 1){
                    session_name('user');
                }else if($info['ID'] == 1){
                    session_name('admin');
                }*/
                var_dump($info);
                $_SESSION['id'] = $info['ID'];
                $_SESSION['username'] = $info['username'];
                $_SESSION['email'] = $info['EMAIL'];
                $_SESSION['checker'] =  $info['EMAIL'].$info['username'].$info['ID'];
                header("location: ../index.php?login=success");
               exit();
            }else{
                header("location: ../index.php?error=wrongpass");
                exit();
            }
            
            }
            else{
            // var_dump($uresult);
            header("location: ../index.php?error=wrongemailorusername");
                exit();
            }
        }
    }
}
else{
    header("location: ../index.php?error=loginfirist");
    exit();
}