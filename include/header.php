
<?php
    session_start();

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta type="description" name="Login"/>
        <meta name="view" content="width=device-width, initial-scale=1"/>
        <meta charset="utf-8"/>
        <link rel="stylesheet" href="style/fontawesome/css/all.css"/>
        <link rel="stylesheet" href="style/style3.css"/>
        <title>Training</title>
    </head>
    <body>
        <div class="wrap">
            <header>
                <nav>
                    <!-- icon fontawesome -->
                    <a href="index.php" class="icon">
                        <i class="fas fa-chart-pie fa-4x"></i>
                    </a>
                    <!--the start of option list-->
                    <ul>
                        <li><a href="index.php">Home</a></li>
                        <li><a href="#">Contact Us</a></li>
                        <li><a href="#" onclick="alert('Designed By Mostafa Abdelrazek');">About Us!</a></li>
                    </ul>
                    <!--the end of option list-->
                </nav>
                <div class="forms">
                    <!-- login form -->
                    <?php
                    if(!isset($_SESSION['id']) && !isset($_SESSION['username'])){
                        echo "
                        <form name=\"loginform\" action=\"include/login.inc.php\" method=\"post\" onsubmit=\"\">
                        <input type=\"text\" placeholder=\"E-mail or Username\" name=\"useremail\" required/>
                        <input type=\"password\" placeholder=\"password\" name=\"userpwd\" required/>
                        <button class=\"b1\" type=\"submit\" name=\"login\" >Login</button>
                    </form>
                    <a class=\"signup-b\" href=\"signup.php\">Signup Now!</a>
                    ";
                }else if(isset($_SESSION['id']) && isset($_SESSION['username'])){
                         echo"
                         <form action=\"include/logout.inc.php\" method=\"post\">
                        <button class=\"b2\"  type=\"submit\" name=\"logout\">Logout</button>
                    </form>";
                        }
                    ?>
                    
                </div>
            </header>
