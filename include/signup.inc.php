<?php

if(isset($_POST['signupsubmit'])){
//    echo "test it's working";
    $unm   = $_POST['username'];
    $email      = $_POST['email'];
    $pwd        = $_POST['pwd'];
    $pwdc       = $_POST['pwdc'];
    if(empty($unm)||empty($pwd)||empty($pwdc)||empty($email)){
        header("location: ../signup.php?error=emptyfields");
        exit;
    }
    else if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
        header("location: ../signup.php?error=invalidemail");
    }
    else if(!preg_match("/^[a-zA-Z0-9]*$/",$unm)||strlen($unm)>15||strlen($unm)<=6){
        header("location: ../signup.php?error=usernameerror");
    }
    else if(strlen($pwd)<8||strlen($pwd)>20||!preg_match("/^[a-zA-Z0-9]*$/",$pwd)){
        header("location: ../signup.php?error=weakpassword");
    }
    else if($pwd !== $pwdc){
        header("location: ../signup.php?error=notmetch");
    }
    else{
        require "dbcon.inc.php";
        $query = "SELECT username ,email FROM USER WHERE USERNAME =? OR EMAIL=?";
        $stmt = mysqli_stmt_init($conn);
        if(!mysqli_stmt_prepare($stmt,$query)){
            header("location: ../signup.php?error=serror");
            exit();
        }
        else{
            mysqli_stmt_bind_param($stmt,'ss',$unm,$email);
            mysqli_stmt_execute($stmt);
            mysqli_stmt_store_result($stmt);
            $resultCheck = mysqli_stmt_num_rows($stmt);
            if($resultCheck > 0){
            header("location: ../signup.php?error=usernameoremailtaken".$email);
            exit();
            }
            else{
                $query = "INSERT INTO user (username,email,password) VALUES (?,?,?)";
                $stmt = mysqli_stmt_init($conn);
                if(!mysqli_stmt_prepare($stmt,$query)){
                    header("location: ../signup.php?error=sIerror");
                    exit();
                }
                else{
                    $hashpass = password_hash($pwd,PASSWORD_DEFAULT);
                    mysqli_stmt_bind_param($stmt,"sss",$unm,$email,$hashpass);
                    mysqli_stmt_execute($stmt);
                    header("location: ../signup.php?signup=success");
                   /* if($_GET['signup'] == 'success'){
                        echo "<script>alter('signup success');</script>";
                    }*/
                }
            }
        }
    }
    mysqli_stmt_close($stmt);
    mysqli_close($conn);
    exit();
}
else{
    header("location:../signup.php");
    exit;
}

 